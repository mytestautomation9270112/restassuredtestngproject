package testScripts;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import commonMethods.APITrigger;
import commonMethods.Testng_Retry_Analyser;
import commonMethods.Utilities;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Post_test_script_DataDriven_testngxml extends APITrigger {
	File logfolder;
	String requestBody;
	Response response;
	ResponseBody responseBody;

	@BeforeTest
	public void setup() {
		logfolder = Utilities.create_folder("Post_API_DDTestngxml");
	}

	@Parameters({ "request_name", "request_job" })
	@Test(retryAnalyzer = Testng_Retry_Analyser.class, description = "Validate the response body parameters of Post_TC1")
	public void validate_PostTC1(String request_name, String request_job) {
		requestBody ="{\r\n"
		+ "    \"name\": \""+request_name+"\",\r\n"
		+ "    \"job\": \""+request_job+"\"\r\n"
		+ "}";
		response = Post_API_trigger(requestBody, post_endpoint());
		int statuscode = response.statusCode();
		responseBody = response.getBody();
		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_id = responseBody.jsonPath().getString("id");
		String res_createdAt = responseBody.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.toString().substring(0, 11);
		// Fetch the request body parameters
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		// Generate expected data.
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		// Validate using TestNG Assertions
		Assert.assertEquals(statuscode, 201, "Correct status code not found even after retrying for 5 times.");

		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
		Assert.assertNotNull(res_id, "Id in ResponseBody is found to be null");
		Assert.assertEquals(res_createdAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");
	}

	@AfterTest
	public void teardown() throws IOException {
		Utilities.create_log_file("Post_API_DDTestngxml", logfolder, post_endpoint(), requestBody,
				response.getHeaders().toString(), responseBody.asString());
	}
}
