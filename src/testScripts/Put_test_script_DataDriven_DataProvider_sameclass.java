package testScripts;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import commonMethods.APITrigger;
import commonMethods.Testng_Retry_Analyser;
import commonMethods.Utilities;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Put_test_script_DataDriven_DataProvider_sameclass extends APITrigger {
	File logfolder;
	String requestBody;
	Response response;
	ResponseBody responseBody;
	
	@DataProvider()
	public Object[][] requestBody() {
		return new Object[][] { { "Emily", "leader" }, { "Ashwini", "Dev" } };
	}

	@BeforeTest
	public void setup() {
		logfolder = Utilities.create_folder("Put_API_DD_DataProvider");
	}

	@Test (retryAnalyzer = Testng_Retry_Analyser.class, dataProvider="requestBody", description = "Validate the response body parameters of Put_TC1")
	public void validate_PutTC1(String request_name, String request_job) {
		requestBody = "{\r\n"
				+ "    \"name\": \""+request_name+"\",\r\n"
				+ "    \"job\": \""+request_job+"\"\r\n"
				+ "}";
		response = Put_API_trigger(requestBody, put_endpoint());
		responseBody = response.getBody();
		int statuscode = response.statusCode();

		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.toString().substring(0, 11);

		// Step 5: Fetch the request body parameters
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// Step 6 : Generate expected data.
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// Step 7 : Validate using TestNG
		Assert.assertEquals(statuscode, 200, "Status code is not equal to expected status code");
		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
		Assert.assertEquals(res_updatedAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");

	}

	@AfterTest
	public void teardown() throws IOException {
		Utilities.create_log_file("Put_API_DD_DataProvider", logfolder, put_endpoint(), requestBody,
				response.getHeaders().toString(), responseBody.asString());
	}

}
