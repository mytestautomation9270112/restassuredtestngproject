package testScripts;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import commonMethods.APITrigger;
import commonMethods.Testng_Retry_Analyser;
import commonMethods.Utilities;
import io.restassured.response.Response;

public class Delete_test_script extends APITrigger {
	File logfolder;
	Response resp;

	@BeforeTest
	public void setup() {
		logfolder = Utilities.create_folder("Delete_API");
	}
	
	@Test (retryAnalyzer = Testng_Retry_Analyser.class, description = "Validate the response body parameters of Delete_TC1")
	public void validate_DeleteTC1() throws IOException {
		resp = Delete_API_trigger(delete_requestBody(), delete_endpoint());
		int status_code = resp.statusCode();
		//System.out.println(status_code);
		Assert.assertEquals(status_code, 204, "Correct status code not found even after retrying for 6 times");
	}
	
	@AfterTest
	public void teardown() throws IOException {
		Utilities.create_log_file("Delete_API_TC1", logfolder, delete_endpoint(), delete_requestBody(),
				resp.getHeaders().toString(), resp.getBody().asString());
	}
}
