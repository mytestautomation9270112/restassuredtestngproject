package environmentAndRepository;

import org.testng.annotations.DataProvider;

public class Testng_DataProvider {
	@DataProvider()
	public Object[][] requestBody() {
		return new Object[][] { { "Amanda", "designer" }, { "Emily", "Tester" } };
	}

}
