package environmentAndRepository;

import java.io.IOException;
import java.util.ArrayList;

import commonMethods.Utilities;

public class RequestRepository extends Environment {

	public static String post_param_requestBody(String testcaseName) throws IOException {

		ArrayList<String> data = Utilities.ReadExcelData("Post_API", testcaseName);
		String key1 = data.get(1);
		String req_name = data.get(2);
		String key2 = data.get(3);
		String req_job = data.get(4);
		String requestBody = "{\r\n" + "    \"" + key1 + "\": \"" + req_name + "\",\r\n" + "    \"" + key2 + "\": \""
				+ req_job + "\"\r\n" + "}";
		return requestBody;
	}

	public static String post_requestBody() {
		String requestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";
		return requestBody;
	}

	public static String post_requestBody_TC2() {
		String requestBody = "{\r\n" + "    \"name\": \"Ashwini\",\r\n" + "    \"job\": \"Tester\"\r\n" + "}";
		return requestBody;
	}

	public static String put_param_requestBody(String testcaseName) throws IOException {

		ArrayList<String> data = Utilities.ReadExcelData("Put_API", testcaseName);
		String req_name = data.get(1);
		String req_job = data.get(2);
		String requestBody = "{\r\n" + "    \"name\": \"" + req_name + "\",\r\n" + "    \"job\": \"" + req_job
				+ "\"\r\n" + "}";
		return requestBody;
	}

	public static String put_requestBody() {
		String requestBody = "{\r\n" + "    \"name\": \"Smith\",\r\n" + "    \"job\": \"manager\"\r\n" + "}";
		return requestBody;
	}

	public static String patch_requestBody() {
		String requestBody = "{\r\n" + "    \"name\": \"Dev\",\r\n" + "    \"job\": \"tester\"\r\n" + "}";
		return requestBody;
	}

	public static String get_requestBody() {
		String requestBody = "";
		return requestBody;
	}

	public static String delete_requestBody() {
		String requestBody = "";
		return requestBody;
	}
}
