Endpoint is : 
https://reqres.in/api/users

Request body is : 
{
    "name": "Ashwini",
    "job": "Dev"
}

Response header is : 
Date=Tue, 21 May 2024 16:00:39 GMT
Content-Type=application/json; charset=utf-8
Content-Length=80
Connection=keep-alive
Report-To={"group":"heroku-nel","max_age":3600,"endpoints":[{"url":"https://nel.heroku.com/reports?ts=1716307239&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=FJMLp7bN19ISvKFJXbBxnnMxjOHT%2FBdi%2BRaOENAcq68%3D"}]}
Reporting-Endpoints=heroku-nel=https://nel.heroku.com/reports?ts=1716307239&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=FJMLp7bN19ISvKFJXbBxnnMxjOHT%2FBdi%2BRaOENAcq68%3D
Nel={"report_to":"heroku-nel","max_age":3600,"success_fraction":0.005,"failure_fraction":0.05,"response_headers":["Via"]}
X-Powered-By=Express
Access-Control-Allow-Origin=*
Etag=W/"50-jyoeXeS8pq7pmdzuKUJMaKTGlLE"
Via=1.1 vegur
CF-Cache-Status=DYNAMIC
Server=cloudflare
CF-RAY=8875c7d55d563e4c-BOM

Response body is : 
{"name":"Ashwini","job":"Dev","id":"811","createdAt":"2024-05-21T16:00:39.460Z"}

