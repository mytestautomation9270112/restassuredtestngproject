Endpoint is : 
https://reqres.in/api/users/2

Request body is : 
{
    "name": "Ashwini",
    "job": "Dev"
}

Response header is : 
Date=Tue, 21 May 2024 16:00:42 GMT
Content-Type=application/json; charset=utf-8
Transfer-Encoding=chunked
Connection=keep-alive
Report-To={"group":"heroku-nel","max_age":3600,"endpoints":[{"url":"https://nel.heroku.com/reports?ts=1716307241&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=TI6cxobu%2FYpkatQ4irZdHpJoahF8mWgWoQP3h0ERP0A%3D"}]}
Reporting-Endpoints=heroku-nel=https://nel.heroku.com/reports?ts=1716307241&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=TI6cxobu%2FYpkatQ4irZdHpJoahF8mWgWoQP3h0ERP0A%3D
Nel={"report_to":"heroku-nel","max_age":3600,"success_fraction":0.005,"failure_fraction":0.05,"response_headers":["Via"]}
X-Powered-By=Express
Access-Control-Allow-Origin=*
Etag=W/"45-+/ni1vUj3JLpvyCEa05X1zdKZlk"
Via=1.1 vegur
CF-Cache-Status=DYNAMIC
Server=cloudflare
CF-RAY=8875c7e53b1d40ed-BOM
Content-Encoding=gzip

Response body is : 
{"name":"Ashwini","job":"Dev","updatedAt":"2024-05-21T16:00:41.993Z"}

